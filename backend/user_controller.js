var db = require('./db_config.js');

exports.getById = function(id, callback){


	var arrIds = id.split(',');

	db.Places.find({
	    'id_4sq': { $in: arrIds}
	}, function(error, place){
		if(error){
			callback({error: 'Não foi possível retornar a lista de locais'});
		}
		else{
			callback(place);
		}
	});	

};

/*exports.create = function(id_4sq, flags, callback){

	var objAux = {
		id_4sq: id_4sq,
		flags: flags
	};

	new db.Places(objAux).save(function(error, place){
		if(error){
			callback({error: 'Erro ao cadastrar dados'});
		}
		else {
			callback(place);
		}
	});

};*/

exports.update = function(id, flags, aquaIndice, name, location, callback){

	/*db.Places.findOneAndUpdate({id_4sq: id}, {flags: flags, aquaIndice: aquaIndice}, {upsert: true}, function(error, place){

		if(error){
			callback(error);
		}
		else {
			callback(place);
		}
	});*/

	var now = new Date();

    db.Places.find({id_4sq: id}, function (err, docs) {
        if (docs.length){

            docs[0].flags = flags;
            docs[0].aquaIndice = aquaIndice;
            docs[0].updated_at = now;
            docs[0].name = name;
            docs[0].location = location;

            docs[0].save(function(error, place){
				if(error){
					callback({error: 'Erro ao cadastrar dados'});
				}
				else {
					callback(place);
				}
			}	);

        }else{

            var objAux = {
				id_4sq: id,
				flags: flags,
				aquaIndice: aquaIndice,
				updated_at :now,
	  			created_at : now,
	  			name : name,
            	location : location
			};

			db.Places(objAux).save(function(error, place){
				if(error){
					callback({error: 'Erro ao cadastrar dados'});
				}
				else {
					callback(place);
				}
			});
        }
    });

};

exports.getHomeVenues = function(callback){

	var query = db.Places.find().limit(10).sort({updated_at: -1});

	query.exec( function(error, places){
		if(error){
			callback({error: 'Não foi possível retornar a lista de locais'});
		}
		else{
			callback(places);
		}});

};