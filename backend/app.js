var app = require('./app_config.js');
var userController = require('./user_controller.js');

app.get('/', function(req, res){

	userController.getHomeVenues(function(response){
		res.json(response);
	});

});

app.get('/places/:id_4sq', function(req, res){

	var id_4sq = req.param('id_4sq');

	userController.getById(id_4sq, function(response){
		res.json(response);
	});

});

/*app.post('/places', function(req, res){

	var id_4sq = req.param('id_4sq');
	var flags = req.param('flags');

	userController.create(id_4sq, flags, function(response){
		res.json(response);
	});

});*/

app.post('/places', function(req, res){

	var venue = req.param('venue');

	console.log(venue);

	var id4Sq = venue.id4Sq;
	var flags = venue.flags;
	var aquaIndice = venue.aquaIndice;
	
	var name = venue.name;
	var location = venue.location;

	userController.update(id4Sq, flags, aquaIndice, name, location, function(response){
		res.json(response);
	});
	

});


app.get('/mock4sq', function(req, res){

		res.json({"meta":{"code":200},"response":{"venues":[{"id":"4fbbeef4e4b0d4cb23c3e7c7","name":"Vegus","contact":{},"location":{"lat":51.77007769186906,"lng":7.912877883509157,"cc":"DE","country":"Alemanha","formattedAddress":["Alemanha"]},"categories":[{"id":"4bf58dd8d48988d1bd941735","name":"Casa de Salada","pluralName":"Casa de Saladas","shortName":"Salada","icon":{"prefix":"https:\/\/ss3.4sqi.net\/img\/categories_v2\/food\/salad_","suffix":".png"},"primary":true}],"verified":false,"stats":{"checkinsCount":6,"usersCount":2,"tipCount":0},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"summary":"Não há ninguém aqui","groups":[]},"referralId":"v-1427372847"},{"id":"4e45334d2271bdbcf67d5767","name":"Vegus Ltda.","contact":{},"location":{"lat":-33.378861,"lng":-70.719291,"cc":"CL","city":"Quilicura","state":"Região Metropolitana de Santiago","country":"Chile","formattedAddress":["Quilicura","Região Metropolitana de Santiago","Chile"]},"categories":[{"id":"4bf58dd8d48988d15f941735","name":"Campo","pluralName":"Campos","shortName":"Campo","icon":{"prefix":"https:\/\/ss3.4sqi.net\/img\/categories_v2\/parks_outdoors\/field_","suffix":".png"},"primary":true}],"verified":false,"stats":{"checkinsCount":2,"usersCount":2,"tipCount":0},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"summary":"Não há ninguém aqui","groups":[]},"referralId":"v-1427372847"},{"id":"4eb51b7602d5f33e926941fd","name":"Vegus Snooker Club Carpark","contact":{},"location":{"lat":13.711342,"lng":100.527205,"cc":"TH","city":"Banguecoque","state":"กรุงเทพมหานคร","country":"Tailândia","formattedAddress":["Banguecoque","Bangkok","Tailândia"]},"categories":[{"id":"4c38df4de52ce0d596b336e1","name":"Estacionamento","pluralName":"Estacionamento","shortName":"Estacionamento","icon":{"prefix":"https:\/\/ss3.4sqi.net\/img\/categories_v2\/building\/parking_","suffix":".png"},"primary":true}],"verified":false,"stats":{"checkinsCount":3,"usersCount":3,"tipCount":0},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"summary":"Não há ninguém aqui","groups":[]},"referralId":"v-1427372847"},{"id":"4fbd9d4be4b0ecb7890fd340","name":"7-ELEVEN (ข้าง UM Tower\/ตรงข้าม Nasa Vegus)","contact":{},"location":{"crossStreet":"Ramkhamhaeng Road","lat":13.741805920217312,"lng":100.60141574337888,"postalCode":"10250","cc":"TH","city":"Banguecoque","state":"กรุงเทพมหานคร","country":"Tailândia","formattedAddress":["Ramkhamhaeng Road","Banguecoque","Bangkok 10250","Tailândia"]},"categories":[{"id":"4d954b0ea243a5684a65b473","name":"Conveniência","pluralName":"Conveniências","shortName":"Conveniências","icon":{"prefix":"https:\/\/ss3.4sqi.net\/img\/categories_v2\/shops\/conveniencestore_","suffix":".png"},"primary":true}],"verified":false,"stats":{"checkinsCount":60,"usersCount":36,"tipCount":0},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"summary":"Não há ninguém aqui","groups":[]},"referralId":"v-1427372847"},{"id":"4f05a3eabe7bf2206ef45dc9","name":"LAS VEGUS","contact":{},"location":{"lat":37.567623138427734,"lng":126.98036193847656,"cc":"KR","country":"República da Coréia","formattedAddress":["명동","República da Coréia"]},"categories":[{"id":"4bf58dd8d48988d116941735","name":"Bar","pluralName":"Bares","shortName":"Bar","icon":{"prefix":"https:\/\/ss3.4sqi.net\/img\/categories_v2\/nightlife\/pub_","suffix":".png"},"primary":true}],"verified":false,"stats":{"checkinsCount":1,"usersCount":1,"tipCount":0},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"summary":"Não há ninguém aqui","groups":[]},"referralId":"v-1427372847"},{"id":"4d8f59ae1716a143a71748f7","name":"Nasa Vegas Hotel (นาซ่าเวกัส)","contact":{"phone":"+6627199888","formattedPhone":"+66 2 719 9888"},"location":{"address":"44 Ramkhamhaeng Rd.","lat":13.743053546032764,"lng":100.60194611549377,"postalCode":"10250","cc":"TH","city":"สวนหลวง","state":"กรุงเทพมหานคร","country":"Tailândia","formattedAddress":["44 Ramkhamhaeng Rd.","Suan Luang","Bangkok 10250","Tailândia"]},"categories":[{"id":"4bf58dd8d48988d1fa931735","name":"Hotel","pluralName":"Hotéis","shortName":"Hotel","icon":{"prefix":"https:\/\/ss3.4sqi.net\/img\/categories_v2\/travel\/hotel_","suffix":".png"},"primary":true}],"verified":false,"stats":{"checkinsCount":5142,"usersCount":2200,"tipCount":51},"url":"http:\/\/nasavegashotel.com","specials":{"count":0,"items":[]},"hereNow":{"count":0,"summary":"Não há ninguém aqui","groups":[]},"referralId":"v-1427372847"},{"id":"4eb51a799a52ad1e3ec8e2a1","name":"Vegus Snooker Club","contact":{},"location":{"lat":13.711389875158563,"lng":100.5274719462832,"cc":"TH","country":"Tailândia","formattedAddress":["Tailândia"]},"categories":[{"id":"4bf58dd8d48988d1e3931735","name":"Bilhar","pluralName":"Bilhar","shortName":"Bilhar","icon":{"prefix":"https:\/\/ss3.4sqi.net\/img\/categories_v2\/arts_entertainment\/billiards_","suffix":".png"},"primary":true}],"verified":false,"stats":{"checkinsCount":35,"usersCount":13,"tipCount":0},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"summary":"Não há ninguém aqui","groups":[]},"referralId":"v-1427372847"},{"id":"4e1b4a001f6e24238ddf07f3","name":"Vegus Desenvolvimento e Participacoes","contact":{"phone":"55395155","formattedPhone":"55395155"},"location":{"address":"Rua Estela, 515","lat":-23.578778532592633,"lng":-46.64612179580028,"postalCode":"04008-090","cc":"BR","city":"São Paulo","state":"SP","country":"Brasil","formattedAddress":["Rua Estela, 515","São Paulo, SP","04008-090","Brasil"]},"categories":[{"id":"4bf58dd8d48988d124941735","name":"Escritório","pluralName":"Escritórios","shortName":"Escritório","icon":{"prefix":"https:\/\/ss3.4sqi.net\/img\/categories_v2\/building\/default_","suffix":".png"},"primary":true}],"verified":false,"stats":{"checkinsCount":141,"usersCount":23,"tipCount":0},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"summary":"Não há ninguém aqui","groups":[]},"referralId":"v-1427372847"},{"id":"4d458037c3e5f04d6a48a520","name":"vegus Sauna","contact":{},"location":{"lat":13.78171,"lng":100.586869,"cc":"TH","country":"Tailândia","formattedAddress":["Tailândia"]},"categories":[{"id":"4bf58dd8d48988d1d8941735","name":"Bar Gay","pluralName":"Bares Gays","shortName":"Bar Gay","icon":{"prefix":"https:\/\/ss3.4sqi.net\/img\/categories_v2\/nightlife\/gaybar_","suffix":".png"},"primary":true}],"verified":false,"stats":{"checkinsCount":3,"usersCount":3,"tipCount":0},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"summary":"Não há ninguém aqui","groups":[]},"referralId":"v-1427372847"},{"id":"4ca8210476d3a093771d136b","name":"Star Vegas Resort Casino (สตาร์เวกัส รีสอร์ท คาสิโน)","contact":{"phone":"+855884946660","formattedPhone":"+855 88 494 6660"},"location":{"address":"Star Vegas Internationnal Resort&Casino","crossStreet":"292 Mao Ze Tung Road","lat":13.665201792526451,"lng":102.55380109225837,"postalCode":"01407","cc":"KH","city":"Paôy Pêt","state":"ខេត្តបន្ទាយមានជ័យ","country":"Camboja","formattedAddress":["Star Vegas Internationnal Resort&Casino (292 Mao Ze Tung Road)","Poipet 01407","Camboja"]},"categories":[{"id":"4bf58dd8d48988d17c941735","name":"Cassino","pluralName":"Cassinos","shortName":"Cassino","icon":{"prefix":"https:\/\/ss3.4sqi.net\/img\/categories_v2\/arts_entertainment\/casino_","suffix":".png"},"primary":true}],"verified":false,"stats":{"checkinsCount":1235,"usersCount":759,"tipCount":18},"url":"http:\/\/www.casino-poipet.com\/starvegas.html","specials":{"count":0,"items":[]},"hereNow":{"count":0,"summary":"Não há ninguém aqui","groups":[]},"referralId":"v-1427372847"},{"id":"51833459498eb65bdf801f5e","name":"วีระ ลาบเป็ด Las Vegus","contact":{},"location":{"lat":36.143898,"lng":-115.193347,"cc":"US","city":"Las Vegas","state":"NV","country":"Estados Unidos","formattedAddress":["Las Vegas, NV","Estados Unidos"]},"categories":[{"id":"4bf58dd8d48988d142941735","name":"Restaurante Asiático","pluralName":"Restaurantes Asiáticos","shortName":"Asiático","icon":{"prefix":"https:\/\/ss3.4sqi.net\/img\/categories_v2\/food\/asian_","suffix":".png"},"primary":true}],"verified":false,"stats":{"checkinsCount":1,"usersCount":1,"tipCount":0},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"summary":"Não há ninguém aqui","groups":[]},"referralId":"v-1427372847"},{"id":"4c008f50ad15a593b02e8e73","name":"Savan Vegas Hotel and Casino","contact":{"phone":"41252200","formattedPhone":"41252200","twitter":"savanvegas1"},"location":{"address":"Nongdeune Village, Kaisonephomvihan District","crossStreet":"(Across the river from Mukdahan)","lat":16.617605194281694,"lng":104.77729797363281,"cc":"TH","city":"Savannakhet, Lao","state":"จังหวัดมุกดาหาร","country":"Tailândia","formattedAddress":["Nongdeune Village, Kaisonephomvihan District ((Across the river from Mukdahan))","Savannakhet, Lao","Changwat Mukdahan","Tailândia"]},"categories":[{"id":"4bf58dd8d48988d17c941735","name":"Cassino","pluralName":"Cassinos","shortName":"Cassino","icon":{"prefix":"https:\/\/ss3.4sqi.net\/img\/categories_v2\/arts_entertainment\/casino_","suffix":".png"},"primary":true}],"verified":true,"stats":{"checkinsCount":403,"usersCount":277,"tipCount":3},"url":"http:\/\/www.savanvegas.com","specials":{"count":0,"items":[]},"hereNow":{"count":0,"summary":"Não há ninguém aqui","groups":[]},"storeId":"","referralId":"v-1427372847"},{"id":"4b5b743cf964a52033ff28e3","name":"FamilyMart (แฟมิลี่มาร์ท)","contact":{},"location":{"address":"inside Nasa Vegas Apartment Bldg.","crossStreet":"Soi Sirimitr Ramkamhang 24\/16","lat":13.742732483153421,"lng":100.60183904669638,"postalCode":"10240","cc":"TH","city":"หัวหมาก","state":"กรุงเทพมหานคร","country":"Tailândia","formattedAddress":["inside Nasa Vegas Apartment Bldg. (Soi Sirimitr Ramkamhang 24\/16)","หัวหมาก","Bangkok 10240","Tailândia"]},"categories":[{"id":"4d954b0ea243a5684a65b473","name":"Conveniência","pluralName":"Conveniências","shortName":"Conveniências","icon":{"prefix":"https:\/\/ss3.4sqi.net\/img\/categories_v2\/shops\/conveniencestore_","suffix":".png"},"primary":true}],"verified":false,"stats":{"checkinsCount":466,"usersCount":174,"tipCount":7},"specials":{"count":0,"items":[]},"hereNow":{"count":0,"summary":"Não há ninguém aqui","groups":[]},"referralId":"v-1427372847"}]}});

});