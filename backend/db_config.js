var dbString = 'mongodb://localhost/AQUAGRAM';

var mongoose = require('mongoose').connect(dbString);

var db = mongoose.connection;

db.on('error', function(){console.log('ERRO ao conectar no db');});

db.once('open', function(){
	console.log('SUCESSO ao conectar no db');

	var PlaceSchema = mongoose.Schema({
		id_4sq: String,
		flags:Array,
		aquaIndice: Number,
		created_at: Date,
		updated_at: Date,
		name: String,
		location: {}
	}, { 
		collection: 'Places' 
	});

	exports.Places = mongoose.model('Places', PlaceSchema);
	
});
