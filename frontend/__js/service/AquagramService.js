var aquagramService = angular.module('AquagramService', []);

aquagramService.factory('AquagramService', ['$resource',
  function($resource){

    //var baseUrl = 'http://192.168.0.105:8080/';
  	var baseUrl = 'http://aquagram.com.br:8080/';
  	var getPlaces = baseUrl + 'places/';

     return {getPlaces: $resource(getPlaces += ':ids4Sq', {}, {
      query: {method:'GET', isArray:true},
      save: {method:'POST', isArray:false}
    }), getVenuesHome: $resource(baseUrl, {}, {
      query: {method:'GET', isArray:true},
    })};

  }]);
