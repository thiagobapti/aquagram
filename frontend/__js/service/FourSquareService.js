var fourSquareService = angular.module('FourSquareService', []);

fourSquareService.factory('FourSquareService', ['$resource',
  function($resource){

  	var apiBase = 'https://api.foursquare.com/v2/';
  	var searchPath = 'venues/search?';
  	var getByIdPath = 'venues';
  	var clientId = 'client_id=CQJCSCLYQTHHHI5BQ0MPAEJSS1QY5CL5W4DFDIFFLE1T1GZD';
  	var clientSecret = '&client_secret=SXVL3JII43XRNFEYAQDR0FXPODLK2S54525WC5TQYVY2BUNJ';
  	var version = '&v=20140701';

  	var searchURL = apiBase + searchPath + clientId + clientSecret + version + '&query=:query';
  	var getByIdURL = apiBase + getByIdPath + '/:id4Sq?'+ clientId + clientSecret + version;

  	if(window.lat && window.long){
		searchURL += '&ll='+window.lat+','+window.long + '&'; 
	}
	else {
		searchURL += '&intent=global&';
	}

  	/*var url = 'https://api.foursquare.com/v2/venues/search?' ;
		url += 'client_id=CQJCSCLYQTHHHI5BQ0MPAEJSS1QY5CL5W4DFDIFFLE1T1GZD&';
		url += 'client_secret=SXVL3JII43XRNFEYAQDR0FXPODLK2S54525WC5TQYVY2BUNJ&'; 
		url += 'v=20140701&'; 
		if(window.lat && window.long){
			url += 'll='+window.lat+','+window.long + '&'; 
		}
		else {
			url += '&intent=global&';
		}
		url += 'query=';*/

    // MOCK !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //searchURL = 'http://192.168.0.105:8080/mock4sq';

    return {search: $resource(searchURL, {}, {
      query: {method:'GET', isArray:false},
      getById: {method:'GET', isArray:false}
    }), getById: $resource(getByIdURL, {}, {
      getById: {method:'GET', isArray:false}
    })};


  }]);
