var me = angular.module('MainController', []);

me.controller('MainController', ['$scope','$location', '$routeParams', '$filter', 'FourSquareService','$route', 'AquagramService', '$translate','$q', '$window','$anchorScroll', '$timeout',
	function($scope, $location, $routeParams, $filter, FourSquareService, $route, AquagramService, $translate, $q, $window, $anchorScroll, $timeout) {

	$scope.language = 'pt';
	$scope.loading = true;
	$scope.error = false;
	$scope.searchQuery = $routeParams.query;
	$scope.venue = {
		id4Sq : $routeParams.id4Sq,
		nome: ''
	};
	//$scope.id4Sq = $routeParams.id4Sq;

	var currentRoutePath = $route.current.$$route.originalPath;

	var setRoute =  function(route){
		$location.path($filter('lowercase')(route));
	};

	$scope.search = function(query){

		if(!query)
			return;

		setRoute('/search/' + query);
		ga('send', 'pageview', '/pesquisar/');
	};

	$scope.venueSelect = function(id4Sq){
		setRoute('/place/' + id4Sq);
		ga('send', 'pageview', '/selecionar/');
	};

	var fourSquareVenueSearch = function (query, successCallback, errorCallback){
		FourSquareService.search.query({query:query}, function(data){
			successCallback(data);
		}, function(error){
			errorCallback(error);
		});
	};

	var fourSquareVenueGetById = function (id4Sq, successCallback, errorCallback){
		FourSquareService.getById.getById({id4Sq:id4Sq}, function(data){
			successCallback(data);
		}, function(error){
			errorCallback(error);
		});
	};

	var aquaIndexSearch = function (ids4Sq, successCallback, errorCallback){
		AquagramService.getPlaces.query({ids4Sq:ids4Sq}, function(data){
			successCallback(data);
		}, function(error){
			errorCallback(error);
		});
	};

	var aquaIndexSave = function (successCallback, errorCallback){
		AquagramService.getPlaces.save({venue:$scope.venue}, function(data){
			successCallback(data);
		}, function(error){
			errorCallback(error);
		});
	};

	var getVenuesHome = function (successCallback, errorCallback){
		AquagramService.getVenuesHome.query(function(data){
			successCallback(data);
		}, function(error){
			errorCallback(error);
		});
	};

	showLoading = function(){
		$timeout(function(){
			$scope.loading = true;
			scrollTop();
		});

	};

	endLoading = function(){
		$timeout(function(){
			$scope.loading = false;
			scrollTop();
		});
	};

	scrollTop = function(){
		window.scrollTo(0, 0);
		 //window.scrollTo(0, 0);
    //$('html').animate({scrollTop:0}, 'slow');//IE, FF
    //$('body').animate({scrollTop:0}, 'slow');//chrome, don't know if Safari works
    //$('.popupPeriod').fadeIn(1000, function(){
		
        //setTimeout(function(){window.scrollTo(0, 0);}, 100);

    //});
};
	

	switch (currentRoutePath){
		case '/':
			getVenuesHome(function(data){
				$scope.venues = data;
				endLoading();
			}, function(error){
				endLoading();
				$scope.error = true;
			});
		break;
		case '/search/:query':
		showLoading();

			fourSquareVenueSearch($scope.searchQuery, function(data){
				$scope.venues = data.response.venues;
				
				// Com base nas venues retornadas pelo 4sq, busco na base aqg por possíveis registros
				var ids_4sq = '';

				for(var i = 0 ; i < $scope.venues.length ; i +=1){
					ids_4sq += $scope.venues[i].id + ',';
				}

				if(ids_4sq){
				aquaIndexSearch(ids_4sq, function(data){

				for(var ii = 0 ; ii < $scope.venues.length ; ii +=1){
					for(var i = 0 ; i < data.length ; i +=1){
						if($scope.venues[ii].id == data[i].id_4sq){
							$scope.venues[ii].aquaIndice = data[i].aquaIndice;
							break;
						}
					}

					if(!$scope.venues[ii].hasOwnProperty('aquaIndice')){
						$scope.venues[ii].aquaIndice = 0;
					}
					endLoading();
				}
				
				}, function(error){

					endLoading();
					$scope.error = true;

				});
					
				}
				else{
					endLoading();

				}

			}, function(error){
				endLoading();
				$scope.error = true;
			});
		break;
		case '/place/:id4Sq':

		var numCalls = 0;

		function placeCallback(data){
			numCalls +=1;

			if(numCalls === 2){
				endLoading();
			}
		}

				aquaIndexSearch($scope.venue.id4Sq, function(data){

				if(data.length > 0){

					$translate('LAST_RATING').then(function(value){
						$scope.venue.updated_at_label = value + ' ' + moment($scope.venue.updated_at).fromNow();
						
					});

					$scope.venue.updated_at = data[0].updated_at;

					setInterval(function(){
						$translate('LAST_RATING').then(function(value){
						$scope.venue.updated_at_label = value + ' ' +  moment($scope.venue.updated_at).fromNow();
						
					});
						$scope.$apply();
					}, 60000);
				}
				else{
					$translate('NEVER_RATED').then(function(value){
						$scope.venue.updated_at_label = value;
						
					});
					
				}

					defineIndicadores(data);
					placeCallback();
				}, function(error){
					$scope.error = true;
				});

				fourSquareVenueGetById($scope.venue.id4Sq, function(data){
					//defineIndicadores(data);
					$scope.venue.name = data.response.venue.name;
					$scope.venue.url = data.response.venue.url;

					$scope.venue.location = data.response.venue.location;
					placeCallback();
				}, function(error){
					$scope.error = true;
				});

		break;
	}

	function defineIndicadores(dbArray){

		$scope.venue.indices = angular.copy(indicadoresBase);

		function retornaFlag(idIndicador){

			if(dbArray.length > 0){

				for(var i = 0 ; i < dbArray[0].flags.length ; i+=1){

					if(dbArray[0].flags[i].id == idIndicador){
						return 1;
					}
				}

			}

			return 0;
		}

		for(var i = 0 ; i < $scope.venue.indices.length ; i+=1){
			$scope.venue.indices[i].valor = retornaFlag($scope.venue.indices[i].id);
		}

		calcAquaIndice(null);
	}

	function calcAquaIndice(callback){

		var indicePositivos = 0;

		for(var i = 0 ; i < $scope.venue.indices.length ; i+=1){

			if ($scope.venue.indices[i].valor == 1){
				indicePositivos +=1;
			}
		}

		$scope.venue.aquaIndice = indicePositivos * 10;

		if(callback){
			callback();
		}
	}

	$scope.changeLanguage = function (key) {
		$scope.language = key;
		ga('send', 'pageview', '/language/');
    	$translate.use(key);
    	moment.locale(key);

    	if(key === 'pt'){
    		indicadoresBase = indicadoresBasePT;
    	}
    	else if(key === 'en'){
    		indicadoresBase = indicadoresBaseEN;
    	}

    	scrollTop();
  	};

  	$scope.avaliar = function(event, idIndicador, valor){
		ga('send', 'pageview', '/avaliar/');

		var arrAux = [];

		for(var i = 0 ; i < $scope.venue.indices.length ; i+=1){
			if($scope.venue.indices[i].id == idIndicador){
				$scope.venue.indices[i].valor = valor;
			}

			if($scope.venue.indices[i].valor == 1){
				arrAux.push({
					id: $scope.venue.indices[i].id,
				 valor: $scope.venue.indices[i].valor
				});
			}
		}

		$scope.venue.flags = arrAux;

		calcAquaIndice(function(){
			aquaIndexSave(function(data){
				$scope.venue.updated_at = data.updated_at;
				$translate('LAST_RATING').then(function(value){
						$scope.venue.updated_at_label = value + ' ' + moment($scope.venue.updated_at).fromNow();
						
					});
			}, function(error){
				$scope.error = true;
			});
		});

	};

	$scope.formataEndereco = function(endereco, cidade, estado, pais){
		var retorno = '', auxCidade, auxEstado, auxPais;

		retorno += endereco || '';
		retorno += (retorno ? (', ' + cidade) || '' : cidade || '');
		retorno += (retorno ? (' - ' + estado) || '' : estado || '');
		retorno += (retorno ? (' ' + pais) || '' : pais || '');

		return retorno;
	};

	$scope.goBack = function(){
		ga('send', 'pageview', '/voltar/');
		$window.history.back();
	};


}]);

var indicadoresBase = [
		{id:"1", desc:'Possui torneiras com desligamento automático?', obs:"A instalação de torneiras com desligamento automático, pode reduzir em até 50% o consumo de água em atividades como lavagem de mãos ou escovação de dentes."},
		{id:"2", desc:'Possui redutor de fluxo nas torneiras?', obs:"Os redutores de fluxo, ao misturarem água e ar, dão a sensação de maior volume do líquido. Dessa forma, enquanto a torneira convencional usa de 14 a 25 litros de água por minuto, a que tem arejador libera de 6 a 10 litros por minuto."},
		{id:"3", desc:'Possui descargas com dois estágios de acionamento?', obs:"A utilização de descargas com dois estágios nos banheiros, pode reduzir o volume gasto em cada descarga de 12 para apenas 3 litros de água."},
		{id:"4", desc:'Faz captação de água de chuva?', obs:"A captação da água da chuva é um importante alidado para atividades que não necessitem de água potável, como limpeza de áreas comuns e jardins."},
		{id:"5", desc:'Trocou a mangueira pelo regador?', obs:"Jardins e plantas regadas por meio de mangueiras consomem um volume muito maior de água frente àquelas que recebem este tratamento por meio de regadores."},
	];

var indicadoresBasePT = [
		{id:"1", desc:'Possui torneiras com desligamento automático?', obs:"A instalação de torneiras com desligamento automático, pode reduzir em até 50% o consumo de água em atividades como lavagem de mãos ou escovação de dentes."},
		{id:"2", desc:'Possui redutor de fluxo nas torneiras?', obs:"Os redutores de fluxo, ao misturarem água e ar, dão a sensação de maior volume do líquido. Dessa forma, enquanto a torneira convencional usa de 14 a 25 litros de água por minuto, a que tem arejador libera de 6 a 10 litros por minuto."},
		{id:"3", desc:'Possui descargas com dois estágios de acionamento?', obs:"A utilização de descargas com dois estágios nos banheiros, pode reduzir o volume gasto em cada descarga de 12 para apenas 3 litros de água."},
		{id:"4", desc:'Faz captação de água de chuva?', obs:"A captação da água da chuva é um importante alidado para atividades que não necessitem de água potável, como limpeza de áreas comuns e jardins."},
		{id:"5", desc:'Trocou a mangueira pelo regador?', obs:"Jardins e plantas regadas por meio de mangueiras consomem um volume muito maior de água frente àquelas que recebem este tratamento por meio de regadores."},
	];

	var indicadoresBaseEN = [
		{id:"1", desc:'Is there automatic shut-off taps?', obs:"[No english version available] A instalação de torneiras com desligamento automático, pode reduzir em até 50% o consumo de água em atividades como lavagem de mãos ou escovação de dentes."},
		{id:"2", desc:'Are there reducer of water flow on taps?', obs:"[No english version available] Os redutores de fluxo, ao misturarem água e ar, dão a sensação de maior volume do líquido. Dessa forma, enquanto a torneira convencional usa de 14 a 25 litros de água por minuto, a que tem arejador libera de 6 a 10 litros por minuto."},
		{id:"3", desc:'Are there two stage flush on toilets?', obs:"[No english version available] A utilização de descargas com dois estágios nos banheiros, pode reduzir o volume gasto em cada descarga de 12 para apenas 3 litros de água."},
		{id:"4", desc:'Is there rain water harvesting?', obs:"[No english version available] A captação da água da chuva é um importante alidado para atividades que não necessitem de água potável, como limpeza de áreas comuns e jardins."},
		{id:"5", desc:'They changed the hose by watering can on gardens?', obs:"[No english version available] Jardins e plantas regadas por meio de mangueiras consomem um volume muito maior de água frente àquelas que recebem este tratamento por meio de regadores."},
	];

