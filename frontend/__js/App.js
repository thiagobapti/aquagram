
var app = angular.module('MainModule', [
	'ngRoute',
	'ngResource',
	'MainController',
    'FourSquareService',
    'AquagramService',
    'pascalprecht.translate'
]);

app.config(['$routeProvider', '$locationProvider', '$translateProvider',
  function($routeProvider, $locationProvider, $translateProvider) {

  	$locationProvider.html5Mode(true);
    $locationProvider.hashPrefix('!');

    $routeProvider.
      when('/', {
        templateUrl: '__view/MainView.html',
        controller: 'MainController',
      }).when('/search/:query', {
        //templateUrl: '__view/SearchView.html',
        templateUrl: '__view/MainView.html',
        controller: 'MainController'
      }).when('/place/:id4Sq', {
        templateUrl: '__view/VenueView.html',
        controller: 'MainController'
      }).otherwise({
        redirectTo: '/'
      });

    $translateProvider.translations('en', {
	    SEARCH: 'Search',
      SEARCH_PLACEHOLDER: 'Search by location name',
      BACK: 'Back',
      YES: 'YES',
	    NO: 'NO',
      LAST_RATING: 'Last rating',
      AQUA_INDEX: 'Aqua-Index',
      ERROR_TITLE: 'It seems that something went wrong :(',
      ERROR_MESSAGE: 'Try to reload the page',
      NEVER_RATED: 'Not rated yet'
 	});
  	$translateProvider.translations('pt', {
	    SEARCH: 'Encontrar',
      SEARCH_PLACEHOLDER: 'Pesquise pelo nome do local',
      BACK: 'Voltar',
      YES: 'SIM',
	    NO: 'NÃO',
      LAST_RATING: 'Última avaliação',
      AQUA_INDEX: 'Aqua-Índice',
      ERROR_TITLE: 'OPS... Isto não era para acontecer :(',
      ERROR_MESSAGE: 'Tente carregar a página novamente',
      NEVER_RATED: 'Este local nunca foi avaliado'
  	});

  	$translateProvider.preferredLanguage('pt');
    moment.locale('pt');
  }]);

